//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
var ishungry = true
var isslepping = true
var isawake = true
var isnothungry = true

    @IBOutlet var sleeppressed: WKInterfaceButton!
    @IBAction func feedpressed() {
        //
        hunger = hunger - 20
        health = health + 5
        health = health + 5

        outputLabel.setText("Health is " + String(health))
        hungerlabel.setText("Health is " + String(hunger))
//

    }
    @IBOutlet var sleepbtn: WKInterfaceButton!
    @IBOutlet var feedbtn: WKInterfaceButton!
    @IBOutlet var showstatuspokemon: WKInterfaceLabel!
    @IBAction func statuspressed() {
        
        
        
        print("Sending message to phone")
        if (WCSession.default.isReachable == true) {
            
            let message = ["name":"Pritesh"] as [String : Any]
            
            // Send the message
            WCSession.default.sendMessage(message, replyHandler:nil)
            
            
            
        }
        else {
            // messagefromphonelbl.setText("cannot reach")
        }
        
        
        
    }
    var nameStr:String?

 var counter = 0
    
    var hunger = 0
    var health = 100
    
    var timer: Timer?

    
    @IBOutlet var hungerlabel: WKInterfaceLabel!
    // MARK: Outlets
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
   
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var outputLabel: WKInterfaceLabel!
    
    
    
    // MARK: Delegate functions
    // ---------------------

    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        // Message from phone comes in this format: ["course":"MADT"]
        let messageBody = message["Name"] as! String
        
       // nameStr = messageBody
      //  print("name issss s IS   = \(nameStr)")

        messageLabel.setText(messageBody)
    }
    
    func createTimer() {
        // 1
        if timer == nil {
            // 2
            timer = Timer.scheduledTimer(timeInterval: 5.0,
                                         target: self,
                                         selector: #selector(updateTimer),
                                         userInfo: nil,
                                         repeats: true)
        }
    }
    @objc func updateTimer() {
        
        
        
        if (health == 5) {
            
            ishungry = false
            
            print("HEALTH is  = \(health)")
            
            
            outputLabel.setText("Health is " + String(health))
            
            showstatuspokemon.setText("is dead")

            timer?.invalidate()
            
        }
        
        print("printing for ever 5 seconds seconmd")
        
        
        hunger = hunger + 10
        
        hungerlabel.setText("Hunger is " + String(hunger))
        
        print("HUNGER IS   = \(hunger)")
           ishungry = false
        
        if (hunger > 80)  {
            
            ishungry = true
            health = health - 5
            
            showstatuspokemon.setText("is hungryyyyy")
            self.feedbtn.setHidden(false)
            
            outputLabel.setText("Health is " + String(health))

            print("HEALTH is  = \(health)")

        }
        else
            
         if (hunger < 80) {
            
            ishungry = false
            
            showstatuspokemon.setText("is idle")
            
            self.sleepbtn.setHidden(false)

            
            print("HEALTH is  = \(health)")

           // timer?.invalidate()

        }
        
        
        
       
//        print(" game is running  \(cpuprogressValue)")
//
//
//        if (cpuprogressValue <= 0) {
//
//            print("gameover")
//            showAlert(withTitle: "sda", message: "sdsdad")
//
//            timer?.invalidate()
//        }
//        else
//        {
//            print(" game is running  \(cpuprogressValue)")
//
//            cpuprogressValue = (cpuprogressValue  - 2)
//
//            cpuprogess.progress = Float(cpuprogressValue)
//
//            //  cpuprogess.progress = cpuprogressValue - 2.00
//
//            // print(" game is running  \(cpuprogressValue)")
//
        
//        }
        
    }
    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        self.feedbtn.setHidden(true)
        self.sleepbtn.setHidden(true)

     
       nameStr =  UserDefaults.standard.string(forKey: "pokName")


          print("name issss s IS   = \(nameStr)" as String?)

        outputLabel.setText("Health is " + String(health))
        hungerlabel.setText("Hunger is " + String(hunger))

        

        
        print("---WATCH APP LOADED")
        self.createTimer()
        
        
        
        
        if (WCSession.isSupported() == true) {
            
      
            
            if (nameStr == "pika") {
                
                // UserDefaults.standard.set("pikachu", forKey: "pokName")
                
                //  print("Pokemon name isss    = \(nameStr)")
                messageLabel.setText(nameStr)
                nameLabel.setText( nameStr! + " is hungry " )

                self.pokemonImageView.setImageNamed("pikachu")
                
            }
            else if (nameStr == "caterpie") {
                
                //  UserDefaults.standard.set("caterpie", forKey: "pokName")
                
                
                
                nameLabel.setText( nameStr! + " is hungry ")

                messageLabel.setText(nameStr)
                
                self.pokemonImageView.setImageNamed("caterpie")
                
            }

            
            
            // create a communication session with the phone
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            messageLabel.setText("WC NOT supported! please tryt again later")
        }
        
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
    // 2. When person presses button on watch, send a message to the phone
    @IBAction func buttonPressed() {
        
        
        let healthstr = String(health)
        let hungerstr = String(hunger)

        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
           // self.messageLabel.setText("Sending msg to phone")
            WCSession.default.sendMessage(
                
              
                ["name": nameStr! + "health " + healthstr + "hunger " + hungerstr] ,

                
                
//                ["name" : nameStr], ["health" : String(health)],["hunger" : String(hunger)],["state of pokemon" : "hungry"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                  //  self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
    }
    
  
    
}
