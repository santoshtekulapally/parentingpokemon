//
//  Screen2Sample.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-31.
//  Copyright © 2019 Parrot. All rights reserved.
//

import Foundation
import WatchKit
import WatchConnectivity

class Screen2Sample: WKInterfaceController, WCSessionDelegate {
    
    var nameStr:String!

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    
    
    @IBAction func gotonext() {
        
        
    }
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        
        // Message from phone comes in this format: ["course":"MADT"]
        let messageBody = message["Name"] as! String
        
        nameStr = messageBody
        
        nameLabel.setText("my name is " + messageBody)
        
        if (nameStr == "pika") {
            
            UserDefaults.standard.set("pika", forKey: "pokName")
            
            print("Pokemon name isss    = \(nameStr)")
            
            self.pokemonImageView.setImageNamed("pikachu")

        }
        else if (nameStr == "caterpie") {
            
            UserDefaults.standard.set("caterpie", forKey: "pokName")
            
            
            
            self.pokemonImageView.setImageNamed("caterpie")

        }
        

    }
    
    // MARK: Outlets
    // ---------------------

    // 1. Outlet for the image view
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    
    // 2. Outlet for the label
    @IBOutlet var nameLabel: WKInterfaceLabel!
    
    // MARK: Delegate functions
    // ---------------------
    
    // Default function, required by the WCSessionDelegate on the Watch side
    
    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if the watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        
    }
    
    

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("---WATCH APP LOADED")
        
        if (WCSession.isSupported() == true) {
            
            nameLabel.setText("please choose a pokemon on phone  and then click on start")
            
            // create a communication session with the phone
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            nameLabel.setText("WC NOT supported! please try again later")
        }
        
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()

      

        
        
    }
    
    
    // MARK: Actions
    @IBAction func startButtonPressed() {
        
        print("Start button pressed")
    }
    
    
    @IBAction func selectNameButtonPressed() {
        print("select name button pressed")
    }
    

}
